<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
                    "http://www.w3.org/TR/html4/loose.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></meta>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"></meta>
	
	<title>Título del proyecto</title>
	
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="icon" type="image/png" href="favicon.png" />
	
	<!-- Reset de estilos para crossbrowser -->
	<link rel="stylesheet" media="screen" type="text/css" href="css/reset.css" />
	
	<!-- CSS de plantillas -->
	<link rel="stylesheet" media="screen" type="text/css" href="css/main.css" />
	<link rel="stylesheet" media="screen" type="text/css" href="css/secciones.css" />
	
	<!--[if IE]>
		<link rel="stylesheet" media="screen" type="text/css" href="css/ie.css" />
	<![endif]-->
	
	<!-- jQuery -->
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	
	<!-- jQuery Cycle -->
	<script type="text/javascript" src="js/jquery.cycle.all.min.js"></script>
	
</head>
<body>
	
	<div id="header">
	</div>
	
	<div id="content">
		<div id="contenido">
			<?php if(!empty($cuerpo)) @include($cuerpo); ?>
			<br class="clear" />
		</div>
	</div>
	
	<div id="footer">
	</div>
</body>
</html>