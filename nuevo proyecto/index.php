<?php
	
	/*==============================================================================================
	index.php : Archivo index
	================================================================================================
	
	Este archivo define cual es el archivo principal del sitio o sistema, así como define la
	variable que modificará el cuerpo del mismo.
	
	Variables:
		
		$cuerpo - archivo a incluir en el sistema
		$slug - nombre de la sección en la que estamos
		
	==============================================================================================*/
	
	//Inicio de sesión
	session_start();
	
	$cuerpo =	'home.php';
	$slug =		'home';
	
	//Si está seteada la variable 'cuerpo' por GET, le agregamos la extensión 'php'
	if(isset($_GET['cuerpo'])):
		
		$cuerpo = 	"$_GET[cuerpo].php";
		$slug =		$_GET['cuerpo'];
	endif;
	
	//Plantilla
	include("template.php");
?>